require 'rails_helper'

RSpec.describe User, type: :model do
  describe "#generate_token" do
    it "calls secure random uuid method" do
      expect(SecureRandom).to receive(:uuid)
      User.generate_token
    end

    it "returns 36 chars uuid" do
      expect(User.generate_token.size).to eq(36)
    end
  end
end

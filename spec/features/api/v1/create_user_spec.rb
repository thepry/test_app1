require 'rails_helper'

describe "User API: create", type: :feature do
  fixtures(:users)

  let(:existing_user) { users(:user1) }
  let(:request_params) do
    {
      email: 'michael@example.com',
      first_name: 'Michael',
      last_name: 'Jackson',
      zipcode: '09876543',
      street: 'Main rd.',
      city: 'Chicago',
      country: 'US'
    }
  end

  it 'returns error if email is empty' do
    post '/api/v1/users/', params: { user: request_params.merge(email: '') }
    json = JSON.parse(response.body)
    expect(json['errors']['email']).to include("can't be blank")
    expect(response).to_not be_success
  end

  it 'returns error if email is not unique' do
    post '/api/v1/users/', params: { user: request_params.merge(email: existing_user.email) }
    json = JSON.parse(response.body)

    expect(response).to_not be_success
    expect(response).to_not have_http_status(:bad_request)
    expect(json['errors']['email']).to include('has already been taken')
  end

  it 'creates user model and returns a token' do
    post '/api/v1/users/', params: { user: request_params }
    json = JSON.parse(response.body)
    user_json = JSON.parse(json['user'])

    expect(response).to be_success

    expect(user_json['token']).to be_present
    expect(user_json['token'].size).to eq 36
    fields = %i(email first_name last_name zipcode street country city)
    fields.each do |field|
      expect(user_json[field.to_s]).to eq request_params[field]
    end
  end
end

require 'rails_helper'

describe "User API: fetch", type: :feature do
  fixtures(:users)
  let(:user) { users(:user1) }

  it 'returns user data' do
    get '/api/v1/users/', params: { token: user.token }
    json = JSON.parse(response.body)
    expect(response).to be_success

    user_json = JSON.parse(json['user'])
    fields = ['email', 'first_name', 'last_name', 'zipcode', 'street', 'country', 'city']
    fields.each do |field|
      expect(user_json[field]).to eq(user[field])
    end
  end

  it "returns not found if user doesn't exists" do
    get '/api/v1/users/', params: { token: 'some-random-token' }
    json = JSON.parse(response.body)
    expect(response).to_not be_success
    expect(response).to have_http_status(:not_found)
  end


  it 'requires token' do
    get '/api/v1/users/'
    json = JSON.parse(response.body)
    expect(response).to_not be_success
    expect(response).to_not have_http_status(:bad_request)
    expect(json['errors']).to have_key('token')
  end
end

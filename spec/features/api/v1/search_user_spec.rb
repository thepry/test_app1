require 'rails_helper'

describe "User API: search", type: :feature do
  fixtures(:users)
  let(:user) { users(:user1) }

  before do
    User.reindex
  end

  it 'searchs users by first_name' do
    get '/api/v1/users/search', params: { name: 'John' }
    json = JSON.parse(response.body)
    expect(response).to be_success

    users = json['users']
    names = users.map { |user| "#{user['first_name']} #{user['last_name']}" }
    expect(names).to eq ["John Doe", "John Bush", "Johny Cage"]
  end

  it "searchs users by last_name" do
    get '/api/v1/users/search', params: { name: 'Cage' }
    json = JSON.parse(response.body)
    users = json['users']
    names = users.map { |user| "#{user['first_name']} #{user['last_name']}" }
    expect(names).to eq ["Johny Cage", "Michael Cage"]
  end
end

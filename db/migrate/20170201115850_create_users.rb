class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :email, null: false
      t.string :first_name
      t.string :last_name
      t.string :zipcode
      t.string :street
      t.string :city
      t.string :country

      t.string :token, null: false

      t.timestamps
    end

    add_index :users, :token, unique: true
    add_index :users, :email, unique: true
  end
end

# Run specs

```
bundle install
rake db:create
rake db:migrate
rspec

```
# Notes:

1. I used rspec for testing
2. `Reform` for user creation data validation
3. I assumed that only email field is required, so presence of other fields is not validated
4. Use `SecureRandom.uuid` as a token generator
5. Added Unique indexes for email and token fields, so it's uniqueness is also controlled on Database level


# To do

1. Specify user json serialization - remote reduntand fields like `id`, `created_at` etc.
2. Use reform for token validation
3. Handle possible email and token collisions
4. Add Rubocop


# README

This is a simple application that allows users to register. The goal is to be
a small coding task. Don't spend more than 1 hour.


## User registration

* Users should be able to register via an API Endpoint.
* A new user model should be created. Users have a unique email, first_name,
  last_name, zipcode, street and country
* This should be programmed with a simple Form object. You can go plain ruby
  object, or a gem, as you like.
* The users then receive a token in their model. That token has to be unique.
* Please write some basic tests.

## View details for the given user

* API Endpoint that returns details for the given user
* A token is required to access this endpoint and give user access.
* Please write a basic test.


Pick the tools you like and feel comfortable with. The goal is for me to see how you program.

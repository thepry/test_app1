class Api::V1::UsersController < ApplicationController
  def show
    return token_is_required unless params[:token]
    user = User.find_by_token(params[:token])
    if user
      render json: { user: user.to_json }, status: :ok
    else
      render json: {}, status: :not_found
    end
  end

  def create
    user = User.new
    form = UserForm.new(user)
    if form.validate(create_params)
      form.sync
      render json: { user: create_user(user).to_json }, status: :ok
    else
      render json: { errors: form.errors.messages }, status: :bad_data
    end
  end

  def search
    users =  User.search(params[:name], fields: [:first_name, :last_name])
    render json: { users: users.to_a }
  end

  private

  def create_user(user)
    user.token = User.generate_token
    user.save!
    user
  end

  def token_is_required
    render json: { errors: { token: 'is required' } }, status: :bad_data
  end

  def create_params
    params.require(:user).permit(:email, :first_name, :last_name, :zipcode, :street, :city, :country)
  end
end

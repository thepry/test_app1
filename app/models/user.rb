require 'securerandom'

class User < ApplicationRecord
  searchkick

  def self.generate_token
    SecureRandom.uuid
  end
end

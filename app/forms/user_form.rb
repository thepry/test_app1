class UserForm < Reform::Form
  include Reform::Form::ActiveRecord
  model :user

  property :email
  validates :email, presence: true
  validates_uniqueness_of :email

  property :first_name
  property :last_name
  property :zipcode
  property :street
  property :city
  property :country
end
